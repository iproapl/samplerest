SampleREST
===============
SpringBootベースのRESTサンプル

Requirements
===============
* Java ... 1.8.0_77。（1.7系でも動くと思うが。）
* Gradle ... ビルドツール。2.12。
* Lombok ... ボイラープレートコードを抑制。1.16.8。[Download](https://projectlombok.org/)

始め方
===============
初回のみ。Eclipseで開発する場合は初期設定が必要。  
まず、コマンドラインでの設定。

*1.gitでリポジトリをcloneする*  

    $ git clone <cloneするリポジトリのurl>

*2.gradle.propertiesを設定する*

内容は以下の通り。ユーザ名、パスワードなどは暫定です。

    org.gradle.daemon=true
    repositoryUser=deployment
    repositoryPass=deployment123
    systemProp.sonar.host.url=http://sonar.hagi.home:8000
    systemProp.sonar.login=admin
    systemProp.sonar.password=admin

設定場所は以下の通り。ホームディレクトリ直下の.gradle/gradle.propertiesです。

    %userprofile%\.gradle\gradle.properties

*3.gradleでeclilpseプロジェクトにする*

    $ cd <cloneしたローカルリポジトリ>
    $ gradle eclipse
    $ gradle eclipseWtp 

*4.eclipseでリポジトリを取り込む*  

importダイアログを開き、「Existing Projects into Workspaces」で取り込む。

次に、Eclipseでの設定。

*5.Gradleプロジェクト用ツールを導入する*  

取り込んだプロジェクトのコンテキストメニューより、Configure→Convert to Gradle Project を選択する。

*6.Springプロジェクト用ツールを導入する*  

取り込んだプロジェクトのコンテキストメニューより、Spring Tools → Add Spring Project Nature を選択する。

完了。


動かし方
===============

*1.アプリを起動する(Eclipseで)*  

Eclipseを起動し、プロジェクトのコンテキストメニューより、[Run As] -> [Gradle Build...] を選択する。

**bootRun**と入力して実行する。

*2.アプリを起動する(コマンドラインで)*

    $ cd <プロジェクトのディレクトリ>
    $ gradle bootRun

以上。
