package myapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

/**
 * アプリケーションのエントリポイント.
 * 
 * warでデプロイする場合があるのでSpringBootServletInitializerを継承する.
 * 
 * http://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#howto-create-a-deployable-war-file
 */
// @Configuration // JavaConfig用のクラスであることを示す.
// @ComponentScan // 本クラスのパッケージ配下のクラスを走査して、@ComponentなどのアンテーションがついているクラスをDIコンテナに登録してくれる
// @EnableAutoConfiguration // Springの設定を自動的にやってくれる
@SpringBootApplication // @Configuration, @EnableAutoConfiguration, @ComponentScan をまとめて指定してくれる。
public class Application extends SpringBootServletInitializer {

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	/**
	 * 動作確認用.
	 * 
	 * @param args 引数
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		// ApplicationContext ctx = SpringApplication.run(Application.class, args);
		// DEBUG DI管理下のBeanを列挙
		//String[] beanNames = ctx.getBeanDefinitionNames();
        //Arrays.sort(beanNames);
        //for (String beanName : beanNames) {
        //    logger.info(beanName);
        //}
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		logger.debug("configure...");
		return application.sources(Application.class);
	}
}
