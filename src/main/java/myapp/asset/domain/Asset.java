package myapp.asset.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import lombok.Data;

/**
 * 資産エンティティ.
 */
@Entity
@Data
public class Asset implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String title;
	@Temporal(TemporalType.DATE)
	@Column(name="BEGIN_DATE")
	private Date beginDate;
	@Column(name="REGISTER_TS")
	private Timestamp registerTS;
	@Column(name="MODIFY_TS")
	private Timestamp modifyTS;
	@Column(name="DELETE_FLAG")
	private boolean deleteFlag;
	@Version
	private int version;
}
