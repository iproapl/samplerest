package myapp.asset.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Assetエンティティ用のリポジトリ.
 * 
 *　JpaRepository継承するインタフェースを定義するだけで、findOne, findAll, save, delete が提供される。
 */
public interface AssetRepository extends JpaRepository<Asset, Long>, JpaSpecificationExecutor<Asset>{
	// findBy + Title + Like
	List<Asset> findByTitleLike(String title);
}
