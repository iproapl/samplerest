package myapp.asset.domain;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

/**
 * Asset用の仕様.
 * 
 * リポジトリからAssetを取得する歳の条件を提供する
 *
 */
public class AssetSpecifications {

	public static Specification<Asset> titleContains(String title) {
		return StringUtils.isEmpty(title) ? null : (root, query, cb) -> {
			return cb.like(root.get("title"), "%" + title + "%");
		};
	}

	public static Specification<Asset> beginDate(Date beginDate) {
		return StringUtils.isEmpty(beginDate) ? null : (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(root.get("beginDate"), beginDate);
		};
	}

	public static Specification<Asset> endDate(Date endDate) {
		return StringUtils.isEmpty(endDate) ? null : (root, query, cb) -> {
			return cb.lessThanOrEqualTo(root.get("beginDate"), endDate);
		};
	}
}
