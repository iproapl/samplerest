package myapp.asset.service;

import java.text.ParseException;
import java.util.Date;

public interface AssetForm {
	
	Long getId();
	
	void setId(Long id);

	Date getConvertedDate() throws ParseException;

	int getConvertedVersion();

	String getVersion();

	void setVersion(String version);

	String getTitle();

	void setTitle(String title);

	String getDate();

	void setDate(String date);

}