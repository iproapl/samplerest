package myapp.asset.service;

import java.text.ParseException;
import java.util.Date;

public interface AssetQuery {

	Date getConvertedBeginDate() throws ParseException;

	Date getConvertedEndDate() throws ParseException;

	String getTitle();

	void setTitle(String title);

	String getBeginDate();

	void setBeginDate(String date);

	String getEndDate();

	void setEndDate(String date);
}