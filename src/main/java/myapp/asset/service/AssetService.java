package myapp.asset.service;

import static myapp.asset.domain.AssetSpecifications.*;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import myapp.asset.domain.Asset;
import myapp.asset.domain.AssetRepository;

/**
 * Asset用のサービスクラス.
 * <p>
 * トランザクション境界を制御します.
 */
@Component
public class AssetService {
	@Autowired
	private AssetRepository assetRepository;

	private static final Logger logger = LoggerFactory.getLogger(AssetService.class);

	public Asset create(AssetForm assetForm) throws ParseException {
		Asset asset = new Asset();
		asset.setTitle(assetForm.getTitle());
		asset.setBeginDate(assetForm.getConvertedDate());
		return assetRepository.save(asset);
		// projectRepository.saveAndFlush(project);
	}

	@Transactional
	public Asset update(AssetForm assetForm) throws ParseException {
		Asset asset = assetRepository.findOne(assetForm.getId());
		// TODO 存在チェック
		if (asset.getVersion() != assetForm.getConvertedVersion()) {
			logger.debug("version不一致.");
			throw new RuntimeException("version不一致.");
		}
		asset.setTitle(assetForm.getTitle());
		asset.setBeginDate(assetForm.getConvertedDate());
		return asset;
	}

	public List<Asset> search() {
		return assetRepository.findAll();
	}

	public List<Asset> search(AssetQuery assertQuery) throws ParseException {
		Specifications<Asset> specs = Specifications.where((titleContains(assertQuery.getTitle())))
				.and(beginDate(assertQuery.getConvertedBeginDate()))
				.and(endDate(assertQuery.getConvertedEndDate()));

		return assetRepository.findAll(specs);
	}

	public Asset delete(Long assetId) {
		Asset asset = assetRepository.findOne(assetId);
		assetRepository.delete(assetId);
		return asset;
	}

	public Asset get(Long assetId) {
		return assetRepository.findOne(assetId);
	}
}
