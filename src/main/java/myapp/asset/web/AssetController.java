package myapp.asset.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Asset用Webコントローラ.
 * <p/>
 * http://localhost:8080/asset などにリクエストすると、asset.html を返す.
 */
@Controller
@RequestMapping("/asset")
public class AssetController {
	@RequestMapping(method = RequestMethod.GET)
	public String top(Model model) {
		return "asset"; // resources/templates/asset.html を指す。
	}
}
