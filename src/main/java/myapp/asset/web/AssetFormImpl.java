package myapp.asset.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import myapp.asset.service.AssetForm;

public class AssetFormImpl implements AssetForm {
	private Long id;
	@NotNull
	@Size(min=1)
	private String title;
	private String date;
	private String version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getConvertedDate()
	 */
	@Override
	public Date getConvertedDate() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		return format.parse(date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getConvertedVersion()
	 */
	@Override
	public int getConvertedVersion() {
		return Integer.parseInt(version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getVersion()
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setVersion(java.lang.String)
	 */
	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getDate()
	 */
	@Override
	public String getDate() {
		return date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setDate(java.lang.String)
	 */
	@Override
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "AssetFormImpl [id=" + id + ", title=" + title + ", date=" + date + ", version=" + version + "]";
	}
}
