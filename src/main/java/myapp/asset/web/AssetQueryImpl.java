package myapp.asset.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

import myapp.asset.service.AssetQuery;

public class AssetQueryImpl implements AssetQuery {
	private String title;
	private String beginDate;
	private String endDate;

	private Date parseDate(String date) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		return format.parse(date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectQuery#getConvertedDate()
	 */
	@Override
	public Date getConvertedBeginDate() throws ParseException {
		if (StringUtils.isEmpty(beginDate)) {
			return null;
		} else {
			return parseDate(beginDate);
		}
	}

	public Date getConvertedEndDate() throws ParseException {
		if (StringUtils.isEmpty(endDate)) {
			return null;
		} else {
			return parseDate(endDate);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectQuery#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectQuery#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectQuery#getDate()
	 */
	@Override
	public String getBeginDate() {
		return beginDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectQuery#setDate(java.lang.String)
	 */
	@Override
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "AssetQueryImpl [title=" + title + ", beginDate=" + beginDate + ", endDate="
				+ endDate + "]";
	}
}
