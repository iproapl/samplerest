package myapp.asset.web;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import myapp.asset.domain.Asset;
import myapp.asset.service.AssetService;

/**
 * Asset用のRestController.
 * 
 */
@RestController
@RequestMapping("/api/asset")
public class AssetRestController {
	@Autowired
	private AssetService assetService;

	private static final Logger logger = LoggerFactory.getLogger(AssetRestController.class);

	/**
	 * 新規作成.
	 * 
	 * @param assettForm
	 *            作成情報
	 * @return 作成したオブジェクト
	 * @throws ParseException
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Asset post(@Validated @RequestBody AssetFormImpl assettForm) throws ParseException {
		logger.debug("create:" + assettForm);

		return assetService.create(assettForm);
	}

	/**
	 * 取得.
	 * 
	 * @param id
	 *            取得対象のid
	 * @return 取得オブジェクト
	 * @throws ParseException
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Asset get(@PathVariable Long id) throws ParseException {
		logger.debug("get:" + id);

		return assetService.get(id);
	}

	/**
	 * 更新.
	 * 
	 * @param id
	 *            更新対象のid
	 * @param assetForm
	 *            更新内容
	 * @return 更新後のオブジェクト
	 * @throws ParseException
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Asset put(@PathVariable Long id, @Validated @RequestBody AssetFormImpl assetForm) throws ParseException {
		logger.debug("update:" + id);
		logger.debug("update:" + assetForm);
		
		assetForm.setId(id);

		return assetService.update(assetForm);
	}

	/**
	 * 削除.
	 * 
	 * @param id
	 *            削除対象のid
	 * @return 削除したオブジェクト
	 */
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public Asset delete(@PathVariable Long id) {
		logger.debug("delete:" + id);

		return assetService.delete(id);
	}

	/**
	 * search処理.
	 * 
	 * GETリクエストで受け取るため、JSON形式で受け取ることができない。 複数項目の検索の時はつかいにくい。
	 * 
	 * @param multiValueMap
	 *            リクエストパラメータ
	 * @return 検索結果
	 */
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public List<Asset> search(@RequestParam MultiValueMap<String, String> multiValueMap) {
		logger.debug("search:" + multiValueMap);

		// Results<Asset> results = new Results<>();
		// results.getRecords().addAll(assetService.search());
		List<Asset> results = assetService.search();

		return results;
	}

	/**
	 * search処理.
	 * 
	 * POSTリクエストで受け取るため、JSON形式で受け取ることができる。
	 * 
	 * @param assetQuery
	 *            検索条件
	 * @return 検索結果
	 * @throws ParseException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<Asset> search(@RequestBody AssetQueryImpl assetQuery) throws ParseException {
		logger.debug("search:" + assetQuery);

		// Results<Asset> results = new Results<>();
		// results.getRecords().addAll(assetService.search(assetQuery));

		List<Asset> results = assetService.search(assetQuery);

		return results;
	}
}
