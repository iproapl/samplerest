package myapp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import myapp.login.service.LoginUserDetailsService;

@Configuration
public class GlobalAuthenticationConfig extends GlobalAuthenticationConfigurerAdapter {

// このやり方だと特定できない
//	@Autowired
//	private UserDetailsService userDetailsService;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
    	// Custom Authentication
        auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
    }
    
    @Bean // DIコンテナ管理対象. デフォルト. Singleton. Bean名はuserDetailsService
    UserDetailsService userDetailsService() {
        return new LoginUserDetailsService();
    }
    
    @Bean // DIコンテナ管理対象. デフォルト. Singleton. Bean名はpasswordEncoder
    PasswordEncoder passwordEncoder() {
    	return new BCryptPasswordEncoder();
    }
    
}