package myapp.login.service;

import lombok.Data;

/**
 * ヘッダー用DTO.
 *
 */
@Data
public class HeaderDto {
	private boolean auth;
	private String nickname;
	private String role;
	private boolean admin; // trueなら管理者.
}
