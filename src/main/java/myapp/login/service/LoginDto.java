package myapp.login.service;

import lombok.Data;

/**
 * ログイン時に使用するDTO.
 */
@Data
public class LoginDto {
	private String username;
	// @JsonIgnore これをしてしまうと、JSON出力時に無視されるだけでなく、設定もできなくなる。
	private String password;
	private String message;
	
	public void maskPassword(){
		this.password = "**********";
	}
}
