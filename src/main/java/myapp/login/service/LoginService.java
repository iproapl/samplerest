package myapp.login.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import myapp.user.domain.User;

@Service
public class LoginService {

	private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

    @Autowired
    private AuthenticationManager authManager;

    /**
     * ログイン処理.
     * <p/>
     * Spring Securityを使用して認証を行います.
     * 
     * @return ページ用DTOインスタンス. 
     */
    public PageDto login(LoginDto loginDto) {
        PageDto pageDto = new PageDto();
        pageDto.setLoginDto(loginDto);
        HeaderDto headerDto = new HeaderDto();
        pageDto.setHeaderDto(headerDto);

        //Spring Security認証処理
        try {
            Authentication request = new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
            logger.debug("request ... " + request);
            Authentication authResult = authManager.authenticate(request);
            logger.debug("authResult ... " + authResult);
            SecurityContextHolder.getContext().setAuthentication(authResult);
            LoginUserDetails principal = (LoginUserDetails)authResult.getPrincipal();
            User user = principal.getUser();
            logger.debug("user ... " + user);
            headerDto.setNickname(user.getNickname());
            headerDto.setAuth(true);
            headerDto.setRole(user.getRole());
            headerDto.setAdmin(user.isAdmin());
        } catch(AuthenticationException e) {
        	logger.debug(e.getMessage(), e);
            loginDto.setMessage("メールアドレスかパスワードが間違っています。");
            headerDto.setAuth(false);
        } finally {
        	// パスワードを消す.JSONデータに含まれるので.
        	loginDto.maskPassword();
        }
        return pageDto;
    }
}
