package myapp.login.service;

import org.springframework.security.core.authority.AuthorityUtils;

import lombok.Getter;
import myapp.user.domain.User;

/**
 * LoginUserDetailsクラス.
 * <p/>
 * UserDetailsインタフェースのデフォルト実装をカスタマイズ.
 */
public class LoginUserDetails extends org.springframework.security.core.userdetails.User {
	private static final long serialVersionUID = 1L;

	@Getter
	private final User user;

	/**
	 * コンストラクタ.
	 * @param user 元になるUserインスタンス. 
	 */
	public LoginUserDetails(User user) {
		// super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList("ROLE_USER"));
		super(user.getUsername(), user.getPassword(), AuthorityUtils.createAuthorityList(user.getRole())); // TODO
		this.user = user;
	}
	
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(user.toString());
        sb.append(", ").append(super.toString());
        return sb.toString();
    }	
}
