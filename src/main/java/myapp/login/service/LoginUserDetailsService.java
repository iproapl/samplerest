package myapp.login.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import myapp.user.domain.User;
import myapp.user.domain.UserRepository;

/**
 * LoginUserDetails用のServiceクラス.
 */
@Service // サービスクラスであることを示す. @ComponentでもOK.
public class LoginUserDetailsService implements UserDetailsService {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginUserDetailsService.class);
	@Autowired
	private UserRepository userRepository;

	/**
	 * ユーザー名に一致するLoginUserDetailsインスタンスをロードする.
	 * 
     * @param username ユーザー名
     * @return UserDetailsインスタンス.(<code>null</code>にはならない.)
     * @throws UsernameNotFoundException ユーザが存在しない場合、権限が付与されていない場合
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.debug("username ... " + username);
		User user = userRepository.findOne(username);
		if (user == null) {
			throw new UsernameNotFoundException("指定されたユーザは存在しません. username = " + username);
		}
		return new LoginUserDetails(user);
	}
}
