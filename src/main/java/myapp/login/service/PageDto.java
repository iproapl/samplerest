package myapp.login.service;

import lombok.Data;

/**
 * ページ用DTO.
 *
 */
@Data
public class PageDto {
	private LoginDto loginDto;
	private HeaderDto headerDto;
}
