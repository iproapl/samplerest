package myapp.login.web;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.WebUtils;

import myapp.login.service.LoginDto;
import myapp.login.service.LoginService;
import myapp.login.service.LoginUserDetails;
import myapp.login.service.PageDto;

/**
 * Login用のRestController.
 */
@RestController
@RequestMapping("/api/")
public class LoginRestController {
	private static final Logger logger = LoggerFactory.getLogger(LoginRestController.class);

    @Autowired
    private LoginService loginService;

    /**
     * ログイン処理.
     * @param loginDto ログイン用Dtoインスタンス
     * @param request リクエスト
     * @param response レスポンス
     * @return ResponseEntitインスタンス.
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    ResponseEntity<PageDto> login(@RequestBody LoginDto loginDto, HttpServletRequest request, HttpServletResponse response) {
    	logger.debug("login..." + loginDto);
        PageDto pageDto = loginService.login(loginDto);
        if (pageDto.getHeaderDto().isAuth()) {
            CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
            logger.debug("CsrfToken : " + csrf);
            if (csrf != null) {
                Cookie cookie = WebUtils.getCookie(request, "XSRF-TOKEN");
                logger.debug("Cookie : " + cookie);
                String token = csrf.getToken();
                logger.debug("CsrfToken.token : " + token);
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                logger.debug("authentication : " + authentication);
                if ((cookie == null || token != null && !token.equals(cookie.getValue()))
                        && (authentication != null && authentication.isAuthenticated())) {
                    cookie = new Cookie("XSRF-TOKEN", token);
                    cookie.setPath("/"); // TODO適切なスコープで
                    response.addCookie(cookie);
                }
            }
            return new ResponseEntity<>(pageDto, null, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(pageDto, null, HttpStatus.UNAUTHORIZED);
        }
    }

    /**
     * ログアウト処理.
     * @param request リクエスト
     */
    @RequestMapping(value = "logout", method = RequestMethod.POST)
    void logout(HttpServletRequest request, HttpServletResponse response, Authentication auth, @AuthenticationPrincipal LoginUserDetails loginUser) {
    	// @CurrentUserってのも使えるはずなのだが。。。
        try {
        	logger.debug(auth==null ? "" : auth.toString());
        	logger.debug(loginUser==null ? "" : loginUser.toString());
            request.logout();
            response.setStatus(HttpStatus.OK.value());
            logger.debug("logout.");
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }
}
