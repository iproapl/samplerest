package myapp.project.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import lombok.Data;

@Entity
@Data
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String code;
	private String title;
	@Temporal(TemporalType.DATE)
	@Column(name="BEGIN_DATE")
	private Date beginDate;
	@Version
	private int version;

	// TODO W2UI用
	public Long getRecid() {
		return id;
	}

	@Override
	public String toString() {
		return "Project [id=" + id + ", code=" + code + ", title=" + title + ", beginDate=" + beginDate + ", version=" + version
				+ "]";
	}
}
