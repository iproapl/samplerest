package myapp.project.domain;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProjectRepository extends JpaRepository<Project, Long>, JpaSpecificationExecutor<Project>{
	// findBy + Code + Like
	List<Project> findByCodeLike(String code);
	
	// findBy + Title + Like
	List<Project> findByTitleLike(String title);

	// findBy + Code + Like + And + Title + Like
	List<Project> findByCodeLikeAndTitleLike(String code, String title);
}
