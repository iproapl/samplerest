package myapp.project.domain;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class ProjectSpecifications {

	public static Specification<Project> codeContains(String code) {
		return StringUtils.isEmpty(code) ? null : (root, query, cb) -> {
			return cb.like(root.get("code"), "%" + code + "%");
		};
	}

	public static Specification<Project> titleContains(String title) {
		return StringUtils.isEmpty(title) ? null : (root, query, cb) -> {
			return cb.like(root.get("title"), "%" + title + "%");
		};
	}

	public static Specification<Project> beginDate(Date beginDate) {
		return StringUtils.isEmpty(beginDate) ? null : (root, query, cb) -> {
			return cb.greaterThanOrEqualTo(root.get("date"), beginDate);
		};
	}

	public static Specification<Project> endDate(Date endDate) {
		return StringUtils.isEmpty(endDate) ? null : (root, query, cb) -> {
			return cb.lessThanOrEqualTo(root.get("date"), endDate);
		};
	}
}
