package myapp.project.service;

import java.text.ParseException;
import java.util.Date;

public interface ProjectForm {

	Long getId();

	Date getConvertedDate() throws ParseException;

	int getConvertedVersion();

	String getRecid();

	void setRecid(String recid);

	String getVersion();

	void setVersion(String version);

	String getCode();

	void setCode(String code);

	String getTitle();

	void setTitle(String title);

	String getDate();

	void setDate(String date);

}