package myapp.project.service;

import java.text.ParseException;
import java.util.Date;

public interface ProjectQuery {

	Date getConvertedBeginDate() throws ParseException;

	Date getConvertedEndDate() throws ParseException;

	String getCode();

	void setCode(String code);

	String getTitle();

	void setTitle(String title);

	String getBeginDate();

	void setBeginDate(String date);

	String getEndDate();

	void setEndDate(String date);
}