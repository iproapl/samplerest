package myapp.project.service;

import static myapp.project.domain.ProjectSpecifications.*;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import myapp.project.domain.Project;
import myapp.project.domain.ProjectRepository;

/**
 * Project用のサービスクラス.
 * <p>
 * トランザクション境界を制御します.
 * 
 * @author hagi
 */
@Component
public class ProjectService {
	@Autowired
	private ProjectRepository projectRepository;

	private static final Logger logger = LoggerFactory.getLogger(ProjectService.class);

	public Project create(ProjectForm projectForm) throws ParseException {
		Project project = new Project();
		project.setCode(projectForm.getCode());
		project.setTitle(projectForm.getTitle());
		project.setBeginDate(projectForm.getConvertedDate());
		return projectRepository.save(project);
		// projectRepository.saveAndFlush(project);
	}

	@Transactional
	public Project update(ProjectForm projectForm) throws ParseException {
		// Project project = projectRepository.getOne(projectQuery.getId());//
		// 上手く更新できず
		Project project = projectRepository.findOne(projectForm.getId());
		if (project.getVersion() != projectForm.getConvertedVersion()) {
			logger.debug("version不一致.");
			throw new RuntimeException("version不一致.");
		}
		project.setCode(projectForm.getCode());
		project.setTitle(projectForm.getTitle());
		project.setBeginDate(projectForm.getConvertedDate());
		return project;
	}

	public List<Project> search() {
		return projectRepository.findAll();
	}

	public List<Project> search(ProjectQuery projectQuery) throws ParseException {
		Specifications<Project> specs = Specifications.where(codeContains(projectQuery.getCode()))
				.and(titleContains(projectQuery.getTitle())).and(beginDate(projectQuery.getConvertedBeginDate()))
				.and(endDate(projectQuery.getConvertedEndDate()));

		return projectRepository.findAll(specs);
	}

	public Project delete(Long projectId) {
		Project project = projectRepository.findOne(projectId);
		projectRepository.delete(projectId);
		return project;
	}

	public Project get(Long projectId) {
		return projectRepository.findOne(projectId);
		// return projectRepository.getOne(projectId);
	}
}
