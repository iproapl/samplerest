package myapp.project.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import myapp.project.service.ProjectQuery;

@Controller
@RequestMapping("/project")
public class ProjectController {
	@RequestMapping(method = RequestMethod.GET)
	public String top(Model model) {
		return "project";
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String formConfirm(@ModelAttribute("query") ProjectQuery query, RedirectAttributes attributes) {
		attributes.addFlashAttribute("CompleteMessage", "abcde");
		System.out.println(query);
		return "redirect:/project";
	}

}
