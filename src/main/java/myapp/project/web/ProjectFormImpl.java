package myapp.project.web;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import myapp.project.service.ProjectForm;

public class ProjectFormImpl implements ProjectForm {
	private String recid;
	private String code;
	private String title;
	private String date;
	private String version;

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getId()
	 */
	@Override
	public Long getId() {
		return Long.parseLong(recid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getConvertedDate()
	 */
	@Override
	public Date getConvertedDate() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		return format.parse(date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getConvertedVersion()
	 */
	@Override
	public int getConvertedVersion() {
		return Integer.parseInt(version);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getRecid()
	 */
	@Override
	public String getRecid() {
		return recid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setRecid(java.lang.String)
	 */
	@Override
	public void setRecid(String recid) {
		this.recid = recid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getVersion()
	 */
	@Override
	public String getVersion() {
		return version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setVersion(java.lang.String)
	 */
	@Override
	public void setVersion(String version) {
		this.version = version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getCode()
	 */
	@Override
	public String getCode() {
		return code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getTitle()
	 */
	@Override
	public String getTitle() {
		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#getDate()
	 */
	@Override
	public String getDate() {
		return date;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see om.web.ProjectForm#setDate(java.lang.String)
	 */
	@Override
	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ProjectFormImpl [recid=" + recid + ", code=" + code + ", title=" + title + ", date=" + date
				+ ", version=" + version + "]";
	}
}
