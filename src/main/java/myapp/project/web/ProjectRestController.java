package myapp.project.web;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import myapp.project.domain.Project;
import myapp.project.service.ProjectService;

/**
 * Project用のRestController.
 * 
 * @author hagi
 */
@RestController
@RequestMapping("/project/rc")
public class ProjectRestController {
	@Autowired
	private ProjectService projectService;

	private static final Logger logger = LoggerFactory.getLogger(ProjectRestController.class);

	@RequestMapping(method = RequestMethod.POST)
	public Project create(@RequestBody ProjectFormImpl projectForm) throws ParseException {
		logger.debug("create:" + projectForm);

		return projectService.create(projectForm);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Project get(@PathVariable Long id) throws ParseException {
		logger.debug("get:" + id);

		return projectService.get(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public Project update(@PathVariable Long id, @RequestBody ProjectFormImpl projectForm) throws ParseException {
		logger.debug("update:" + id);
		logger.debug("update:" + projectForm);

		return projectService.update(projectForm);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Project delete(@PathVariable Long id) {
		logger.debug("delete:" + id);

		return projectService.delete(id);
	}

	/**
	 * search処理. GETリクエストで受け取るため、JSON形式で受け取ることができない。 複数項目の検索の時はつかいにくい。
	 * 
	 * @param multiValueMap
	 *            リクエストパラメータ
	 * @return 検索結果
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<Project> search(@RequestParam MultiValueMap<String, String> multiValueMap) {
		logger.debug("search:" + multiValueMap);

		return projectService.search();
	}

	/**
	 * search処理. POSTリクエストで受け取るため、JSON形式で受け取ることができる。
	 * 
	 * @param projectQuery
	 *            検索条件
	 * @return 検索結果
	 * @throws ParseException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<Project> search(@RequestBody ProjectQueryImpl projectQuery) throws ParseException {
		logger.debug("search:" + projectQuery);

		return projectService.search(projectQuery);
	}
}
