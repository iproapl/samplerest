package myapp.user.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Userクラス.
 */
@Data // 各フィールドのgetter/setter, toStringメソッド, equalsメソッド, hashCodeメソッドを生成する.
@NoArgsConstructor // 引数なしコンストラクタを生成する. JPAのEntityには必要.
@AllArgsConstructor // すべてのフィールドを引数に持つコンストラクタを生成する.
@Entity // JPAのエンティティであることを示す.
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id // エンティティのID
	private String username;
	// @JsonIgnore // REST API でJSON形式に変換する際にpasswordフィールドは含めない.無視する.
	private String password; // TODO 暗号化
	private String nickname;
	private String role;

	@Column(name="REGISTER_TS")
	private Timestamp registerTS;
	@Column(name="MODIFY_TS")
	private Timestamp modifyTS;
	@Column(name="DELETE_FLAG")
	private boolean deleteFlag;
	@Version
	private int version;

	// roleが'ROLE_ADMIN'の場合にtrue
	public boolean isAdmin() {
		return "ROLE_ADMIN".equals(role); // TODO
	}
}
