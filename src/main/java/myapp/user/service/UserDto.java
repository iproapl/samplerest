package myapp.user.service;

import lombok.Data;

@Data
public class UserDto {
	private String username;
	private String password;
	private String role;
	private String nickname;
	private int version;
}
