package myapp.user.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import myapp.user.domain.User;
import myapp.user.domain.UserRepository;

@Service
public class UserService {
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Transactional
	public User create(UserDto userDto) {
		User user = modelMapper.map(userDto, User.class);
		return userRepository.save(user);
	}
	
	public User read(String username) {
		return userRepository.findOne(username);
	}
	
	//@Transactional
	public User update(UserDto userDto) throws Exception {
		User user = userRepository.getOne(userDto.getUsername());
		if(user.getVersion() != userDto.getVersion()) {
			logger.debug("version不一致.");
			throw new RuntimeException("version不一致.");
		}
		logger.debug("before..." + user);
		modelMapper.map(userDto, user);
		logger.debug("after..." + user);
		//throw new Exception("error!");
		return user;
	}
	
	public void delete(String username) {
		userRepository.delete(username);
	}
	
	public List<User> findAll(){
		return userRepository.findAll();
	}

	public List<User> search(UserQueryDto userQueryDto) {
		// TODO Auto-generated method stub
		return findAll();
	}
}
