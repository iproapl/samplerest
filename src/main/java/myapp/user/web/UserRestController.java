package myapp.user.web;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import myapp.user.domain.User;
import myapp.user.service.UserDto;
import myapp.user.service.UserQueryDto;
import myapp.user.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserRestController {
	@Autowired
	private UserService userService;

	private static final Logger logger = LoggerFactory.getLogger(UserRestController.class);

	/**
	 * 新規作成.
	 * 
	 * @param userDto
	 *            作成情報
	 * @return 作成したオブジェクト
	 */
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public User post(@Validated @RequestBody UserDto userDto) {
		logger.debug("create:" + userDto);

		return userService.create(userDto);
	}

	/**
	 * 取得.
	 * 
	 * @param id
	 *            取得対象のid
	 * @return 取得オブジェクト
	 */
	@RequestMapping(value = "{username}", method = RequestMethod.GET)
	public User get(@PathVariable String username) {
		logger.debug("get:" + username);

		return userService.read(username);
	}

	/**
	 * 更新.
	 * 
	 * @param id
	 *            更新対象のid
	 * @param userDto
	 *            更新内容
	 * @return 更新後のオブジェクト
	 * @throws Exception 
	 * @throws ParseException
	 */
	@RequestMapping(value = "{username}", method = RequestMethod.PUT)
	public User put(@PathVariable String username, @Validated @RequestBody UserDto userDto) throws Exception {
		logger.debug("update:" + username);
		logger.debug("update:" + userDto);
		
		userDto.setUsername(username);

		return userService.update(userDto);
	}

	/**
	 * 削除.
	 * 
	 * @param id
	 *            削除対象のid
	 * @return 削除したオブジェクト
	 */
	@RequestMapping(value = "{username}", method = RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable String username) {
		logger.debug("delete:" + username);

		userService.delete(username);
	}

	/**
	 * search処理.
	 * 
	 * POSTリクエストで受け取るため、JSON形式で受け取ることができる。
	 * 
	 * @param userQueryDto
	 *            検索条件
	 * @return 検索結果
	 * @throws ParseException
	 */
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<User> search(@RequestBody UserQueryDto userQueryDto) {
		logger.debug("search:" + userQueryDto);

		return userService.search(userQueryDto);
	}

}
