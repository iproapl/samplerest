CREATE TABLE USER (
  USERNAME VARCHAR(100) PRIMARY KEY,
  PASSWORD VARCHAR(255),
  NICKNAME VARCHAR(100),
  ROLE VARCHAR(10),
  REGISTER_TS TIMESTAMP,
  MODIFY_TS TIMESTAMP,
  DELETE_FLAG BOOLEAN DEFAULT FALSE,
  VERSION INT NOT NULL DEFAULT 0
);

INSERT INTO USER (USERNAME, PASSWORD, NICKNAME, ROLE) VALUES ('user1', '$2a$10$0FjuISB1oXnb9NUcICaY/u3g5M7XfRbzy.R.ODnc9lcu3D9aPKA7C', 'ユーザ1', 'ROLE_USER');// パスワードは'demo'
INSERT INTO USER (USERNAME, PASSWORD, NICKNAME, ROLE) VALUES ('user2', '$2a$10$0FjuISB1oXnb9NUcICaY/u3g5M7XfRbzy.R.ODnc9lcu3D9aPKA7C', 'ユーザ2', 'ROLE_USER');
INSERT INTO USER (USERNAME, PASSWORD, NICKNAME, ROLE) VALUES ('admin', '$2a$10$0FjuISB1oXnb9NUcICaY/u3g5M7XfRbzy.R.ODnc9lcu3D9aPKA7C', 'アドミン', 'ROLE_ADMIN');


CREATE TABLE ASSET (
 ID LONG PRIMARY KEY AUTO_INCREMENT,
 TITLE VARCHAR(100) NOT NULL,
 BEGIN_DATE DATE NOT NULL,
 REGISTER_TS TIMESTAMP,
 MODIFY_TS TIMESTAMP,
 DELETE_FLAG BOOLEAN DEFAULT FALSE,
 VERSION INT NOT NULL DEFAULT 0
);

INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト１', '2015-11-1', '2015-11-1 00:00:00');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト２', '2015-12-1', '2015-12-1 00:00:00');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト３', '2016-1-1', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト４', '2016-1-2', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト５', '2016-1-3', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト６', '2016-1-4', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト７', '2016-1-5', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト８', '2016-1-6', '2016-1-1 01:02:03');
INSERT INTO ASSET (TITLE, BEGIN_DATE, REGISTER_TS) VALUES ('テスト９', '2016-1-7', '2016-1-1 01:02:03');



CREATE TABLE PROJECT (
 ID LONG PRIMARY KEY AUTO_INCREMENT,
 CODE VARCHAR(10) NOT NULL,
 TITLE VARCHAR(100) NOT NULL,
 BEGIN_DATE DATE NOT NULL,
 VERSION INT NOT NULL
);

INSERT INTO PROJECT (CODE, TITLE, BEGIN_DATE, VERSION) VALUES ('a1', 'テスト', '2015-11-1', 0);
INSERT INTO PROJECT (CODE, TITLE, BEGIN_DATE, VERSION) VALUES ('a2', 'テスト２', '2015-11-10', 0);
INSERT INTO PROJECT (CODE, TITLE, BEGIN_DATE, VERSION) VALUES ('a3', 'テスト３', '2015-11-15', 0);
INSERT INTO PROJECT (CODE, TITLE, BEGIN_DATE, VERSION) VALUES ('a5', 'テスト４', '2015-11-20', 0);
INSERT INTO PROJECT (CODE, TITLE, BEGIN_DATE, VERSION) VALUES ('a5', 'テスト５', '2015-11-20', 0);
