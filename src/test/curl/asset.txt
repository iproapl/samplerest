# get all
curl "http://localhost:8080/api/asset/search"

# get one
curl http://localhost:8080/api/asset/1

# search
curl -X POST -H "Content-Type: application/json" -d '{"title":"テスト", "endDate":"2015/12/31"}' http://localhost:8080/api/asset/search

# create 
curl -X POST -H "Content-Type: application/json" -d '{"title":"テスト１０", "date":"2016/1/2"}' http://localhost:8080/api/asset

# modify
curl -X PUT -H "Content-Type: application/json" -d '{"title":"bbb", "date":"2015/1/1", "version":"0"}' http://localhost:8080/api/asset/1

# delete
curl -X DELETE http://localhost:8080/api/asset/1
