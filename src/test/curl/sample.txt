# POSTでjsonでアクセスする
curl -X POST -H "Content-Type: application/json" -d '{"code":"a", "title":"b", "date":"c"}' http://localhost:8080/project/rc/search

curl -X POST -H "Content-Type: application/json" -d '{"code":"a", "title":"b", "date":"2015/1/1", "version":"1"}' http://localhost:8080/project/rc/save

curl -X POST -H "Content-Type: application/json" -d '{"recid":"8", "code":"aaa", "title":"bbb", "date":"2015/1/1", "version":"1"}' http://localhost:8080/project/rc/save

curl -X PUT -H "Content-Type: application/json" -d '{"recid":"17", "code":"aaa", "title":"bbb", "date":"2015/1/1", "version":"1"}' http://localhost:8080/project/rc/17
