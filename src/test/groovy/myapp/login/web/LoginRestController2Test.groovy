package myapp.login.web;

import static com.jayway.restassured.RestAssured.given
import static org.hamcrest.CoreMatchers.*

import com.jayway.restassured.specification.RequestSpecification

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.IntegrationTest
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.boot.test.WebIntegrationTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

import com.jayway.restassured.RestAssured
import com.jayway.restassured.filter.session.SessionFilter;
import com.jayway.restassured.http.ContentType

import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

import myapp.Application
import myapp.login.service.LoginDto

// spockを使う場合は、　SpringJUnit4ClassRunner + SpringApplicationConfigurationじゃなくて、
// ContextConfigurationでloaderを指定する
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringApplicationConfiguration(classes=Application.class) // テスト用のApplicationContextを生成する
@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class] )
@WebAppConfiguration // Webのテストを示し
@IntegrationTest("server.port:0") // 空いているポートをリッスンさせる
class LoginRestController2Test extends Specification {
	
	@Value('${local.server.port}') // シングルクォートじゃなきゃダメ
	int port;
	
	// 各テスト前に実行される
	def setup() {
		RestAssured.port = port
	}
	
	// 各テスト後に実行される
	def cleanup() {
		RestAssured.reset();
	}

	@Unroll
	@Test
	def "#userでログインする"() {
		// ResponseBodyこんな感じ
		// {"loginDto":{"username":"user1","password":"**********","message":null},"headerDto":{"auth":true,"nickname":"ユーザ1","role":"ROLE_USER","admin":false}}
		given:
			LoginDto loginDto = new LoginDto()
			loginDto.setUsername(user)
			loginDto.setPassword(password)
			// @formatter:off
			RequestSpecification rs = given()
			.contentType(ContentType.JSON)
			.body(loginDto)
			// @formatter:on
		when:
			// @formatter:off
			rs.when()
			.post("api/login")
			// @formatter:on
		then:
			// @formatter:off
			rs.then()
			.contentType(ContentType.JSON)
			.statusCode(HttpStatus.OK.value())
			.body("loginDto.username", is(username))
			.body("loginDto.password", is("**********"))
			.body("loginDto.message", is(nullValue()))
			.body("headerDto.nickname", is(nickname))
			.body("headerDto.role", is(role))
			.body("headerDto.auth", is(auth))
			.body("headerDto.admin", is(admin))
			// @formatter:on
			
		where:
			// @formatter:off
			user    | password || username | nickname | auth | role         | admin 
			"user1" | "demo"   || "user1"  | "ユーザ1"  | true | "ROLE_USER"  | false
			"user2" | "demo"   || "user2"  | "ユーザ2"  | true | "ROLE_USER"  | false
			"admin" | "demo"   || "admin"  | "アドミン" | true | "ROLE_DMIN" | true
			// @formatter:on
	}

	@Test
	def "ログインに失敗する"() {
		// @formatter:off
		expect:
			LoginDto loginDto = new LoginDto();
			loginDto.setUsername("admin");
			loginDto.setPassword("demoo"); // パスワードNG
			given()
				.body(loginDto)
				.contentType(ContentType.JSON)
				.when()
				.post("api/login")
				.then()
				.statusCode(HttpStatus.UNAUTHORIZED.value())
				.contentType(ContentType.JSON)
				.body("loginDto.username", is("admin"))
				.body("loginDto.password", is("**********"))
				.body("loginDto.message", is(notNullValue()))
				.body("headerDto.nickname", is(nullValue()))
				.body("headerDto.role", is(nullValue()))
				.body("headerDto.auth", is(false))
				.body("headerDto.admin", is(false))
		// @formatter:on
	}	

	def "ログインしてログアウトする"() {
		// @formatter:off
		expect:
			LoginDto loginDto = new LoginDto();
			loginDto.setUsername("admin");
			loginDto.setPassword("demo");
			//
			SessionFilter sessionFilter = new SessionFilter();
			// login
			String token = given()
				.filter(sessionFilter) // sessionidをキャプチャしておいて、別のリクエストで利用する
				.contentType(ContentType.JSON)
				.body(loginDto)
			.when()
				.post("/api/login")
				.getCookie("XSRF-TOKEN") // CSRF対策用
		
			// logout
			given()
				.header("X-XSRF-TOKEN",token) // CSRF対策用
				.filter(sessionFilter) // キャプチャしといたsessionidを利用
				.contentType(ContentType.JSON)
			.when()
				.post("api/logout")
			.then()
				.statusCode(HttpStatus.OK.value())
		// @formatter:on
	}
}