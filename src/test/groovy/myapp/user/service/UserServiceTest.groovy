package myapp.user.service

import static org.junit.Assert.*

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationContextLoader
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import spock.lang.Stepwise

import myapp.Application
import myapp.user.domain.User

@ContextConfiguration(loader = SpringApplicationContextLoader.class, classes = [Application.class] )
@Stepwise // featureを定義順に実行させる
class UserServiceTest extends Specification {
	
	@Autowired
	UserService userService

	@Test
	public void "登録する"() {
		setup:
			UserDto userDto = new UserDto()
			userDto.username = "admin"
			userDto.password = "bbb"
			userDto.role = ""
			userDto.nickname = ""
		when:
			userService.create(userDto)
		then:
			User result = userService.read("hoge")
			//result.username == "hoge"
	}

	@Test
	public void "登録する2"() {
		setup:
			UserDto userDto = new UserDto()
			userDto.username = "admin"
			userDto.password = "aaa"
			userDto.role = ""
			userDto.nickname = ""
			userDto.version = 1
		when:
			userService.create(userDto)
		then:
			User result = userService.read("hoge")
			//result.username == "hoge"
	}
	
	@Test
	def "検索する"() {
		setup:
		
		when:
			User result = userService.read("admin")
			result.nickname = "aaaaa"
		then:
			result.nickname == "aaaaa"
	}

	@Test
	def "更新する"() {
		setup:
			UserDto userDto = new UserDto()
			userDto.username = "admin"
			userDto.nickname = "hogehoge"
			userDto.version = 2
		when:
			try{
			userService.update(userDto)
			}catch(Exception e){
				println e
			}
			
		then:
			User result = userService.read("admin")
		//	result.nickname == "hogehoge"
	}

	@Test
	public void "全て取得する"() {
		setup:

		when:
			List<User> users = userService.findAll()
			users.each{println it}
		
		then:
			users.size == 3
	}
}
