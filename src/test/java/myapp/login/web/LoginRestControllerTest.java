package myapp.login.web;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.After;
import org.junit.Before;

import static org.hamcrest.CoreMatchers.*;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.filter.session.SessionFilter;
import com.jayway.restassured.http.ContentType;

import static com.jayway.restassured.RestAssured.*;
import static com.jayway.restassured.matcher.RestAssuredMatchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import myapp.Application;
import myapp.login.service.LoginDto;

/**
 * サーバーを起動し空いているポートでリクエストをリッスンしておいて、動作確認するやり方.
 * <p/>
 * サーバ環境でのE2Eテスト(REST-DB).
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=Application.class) // テスト用のApplicationContextを生成する
@WebAppConfiguration // Webのテストを示し
@IntegrationTest("server.port:0") // 空いているポートをリッスンさせる
public class LoginRestControllerTest {
	@Value("${local.server.port}")
	int port;

	@Before
	public void setup() {
		// 
		RestAssured.port = port;
	}
	
	@After
	public void teardown() {
	}
	
	@Test
	public void ユーザでログインする() {
		//
		LoginDto loginDto = new LoginDto();
		loginDto.setUsername("user1");
		loginDto.setPassword("demo");
		// ResponseBodyこんな感じ
		// {"loginDto":{"username":"user1","password":"**********","message":null},"headerDto":{"auth":true,"nickname":"ユーザ1","role":"ROLE_USER","admin":false}}
		// @formatter:off
		given()
			.contentType(ContentType.JSON)
			.body(loginDto)
		.when()
			.post("api/login")
		.then()
			.contentType(ContentType.JSON)
			.statusCode(HttpStatus.OK.value())
			.body("loginDto.username", is("user1"))
			.body("loginDto.password", is("**********"))
			.body("loginDto.message", is(nullValue()))
			.body("headerDto.nickname", is("ユーザ1"))
			.body("headerDto.role", is("ROLE_USER"))
			.body("headerDto.auth", is(true))
			.body("headerDto.admin", is(false));
		// @formatter:on
	}

	@Test
	public void 管理者でログインする() {
		//
		LoginDto loginDto = new LoginDto();
		loginDto.setUsername("admin");
		loginDto.setPassword("demo");
		// @formatter:off
		given()
			.contentType(ContentType.JSON)
			.body(loginDto)
		.when()
			.post("api/login")
		.then()
			.contentType(ContentType.JSON)
			.statusCode(HttpStatus.OK.value()) // 
			.body("loginDto.username", is("admin"))
			.body("loginDto.password", is("**********"))
			.body("loginDto.message", is(nullValue()))
			.body("headerDto.nickname", is("アドミン"))
			.body("headerDto.role", is("ROLE_ADMIN"))
			.body("headerDto.auth", is(true))
			.body("headerDto.admin", is(true));
		// @formatter:on
	}

	@Test
	public void ログイン失敗する() {
		//
		LoginDto loginDto = new LoginDto();
		loginDto.setUsername("admin");
		loginDto.setPassword("demoo"); // パスワードNG
		// @formatter:off
		given()
			.body(loginDto)
			.contentType(ContentType.JSON)
		.when()
			.post("api/login")
		.then()
			.statusCode(HttpStatus.UNAUTHORIZED.value())
			.contentType(ContentType.JSON)
			.body("loginDto.username", is("admin"))
			.body("loginDto.password", is("**********"))
			.body("loginDto.message", is(notNullValue()))
			.body("headerDto.nickname", is(nullValue()))
			.body("headerDto.role", is(nullValue()))
			.body("headerDto.auth", is(false))
			.body("headerDto.admin", is(false));
		// @formatter:on
	}

	/**
	 * 認証済みでないと弾かれるので、ログインしてからログアウトする.
	 */
	@Test
	public void ログアウトする() {
		// System.out.println(getPrincipal().toString());
		LoginDto loginDto = new LoginDto();
		loginDto.setUsername("admin");
		loginDto.setPassword("demo");
		//
		SessionFilter sessionFilter = new SessionFilter();
		
		// login
		// @formatter:off
		String token = given()
			.filter(sessionFilter) // sessionidをキャプチャしておいて、別のリクエストで利用する
			.contentType(ContentType.JSON)
			.body(loginDto)
		.when()
			.post("/api/login")
			.getCookie("XSRF-TOKEN"); // CSRF対策用
		// @formatter:on
		
		// logout
		// @formatter:off
		given()
			.header("X-XSRF-TOKEN",token) // CSRF対策用
			.filter(sessionFilter) // キャプチャしといたsessionidを利用
			.contentType(ContentType.JSON)
		.when()
			.post("api/logout")
		.then()
			.statusCode(HttpStatus.OK.value());
		// @formatter:on
		
//		System.out.println(getPrincipal().toString());
	}
	
	/*
	private Object getPrincipal() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
	*/
	
}
